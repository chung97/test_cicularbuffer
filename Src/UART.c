/**/
#include "UART.h"
#include "cicular.h"

extern bool	g_rev_index;
uint16_t	s_rev_pre_index = 0;
extern bool	g_flagRxDone, g_flagRx;
extern cicularBuf_t g_cBuffer;
extern uint16_t numByteRx;
extern UART_HandleTypeDef huart1;

uint16_t UART_ReadData(uint8_t *str, uint16_t len)
{
	uint16_t cnt = 0;
	while(CBUFFER_Empty(&g_cBuffer) == false && cnt < len)
	{
		CBUFFER_Get(&g_cBuffer, &str[cnt]);
		cnt++;
	}
	
//	g_rev_index = 0;
//	s_rev_pre_index = 0;
	g_flagRxDone = false;
	g_flagRx = false;
	return cnt;
}

/* 1ms check 1 lan xem da xong chuoi chua */
/* khi nhan duoc 1 ky tu thi g_rev_index != s_rev_pre_index, g_flagRx = true */
/* */
void UART_CheckDone(void)
{
	if(g_rev_index == s_rev_pre_index && g_flagRx == true)
	{
		g_flagRxDone = true;
	}

	s_rev_pre_index = g_rev_index;
}

//void UART_Process(uint8_t *str)
//{
//	if(g_flagRxDone == true)
//	{
//		numByteRx = UART_ReadData(str, 150);
//		HAL_UART_Transmit(&huart1, (uint8_t*)str, numByteRx, 100);
//	}
//}
