
#ifndef _UART_H
#define _UART_H

#include "stm32f3xx_hal.h"

uint16_t UART_ReadData(uint8_t *str, uint16_t len);
void UART_CheckDone(void);
//void UART_Process(uint8_t *str);
#endif
